#! /usr/bin/env python3


# udd-to-yaml -- Convert Universal Debian Database to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/codegouv/udd-to-yaml
#
# udd-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# udd-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import logging
import os
import pickle
import pprint
import sys

import psycopg2
import psycopg2.extras


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
log = logging.getLogger(app_name)
package_by_name = {}
source_by_name = {}


def encode_str_in_item(d, key):
    value = d.get(key)
    if value is not None:
        d[key] = value.encode('latin1').decode('utf-8')


def load_package(name):
    assert name is not None
    package = package_by_name.get(name)
    if package is not None:
        return package
    save_packages()
    package_path = os.path.join(
        args.pickle_dir,
        'packages',
        name[0:4] if name.startswith('lib') else name[0],
        '{}.pickle'.format(name),
        )
    if not os.path.exists(package_path):
        package_by_name[name] = package = dict(name = name)
        return package
    with open(package_path, 'rb') as package_file:
        package = pickle.load(package_file)
    package_by_name[name] = package
    return package


def load_source(name):
    assert name is not None
    source = source_by_name.get(name)
    if source is not None:
        return source
    save_sources()
    source_path = os.path.join(
        args.pickle_dir,
        'sources',
        name[0:4] if name.startswith('lib') else name[0],
        '{}.pickle'.format(name),
        )
    if not os.path.exists(source_path):
        source_by_name[name] = source = dict(name = name)
        return source
    with open(source_path, 'rb') as source_file:
        source = pickle.load(source_file)
    source_by_name[name] = source
    return source


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pickle_dir', help = 'path of target directory for files in pickle format')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    if not os.path.exists(args.pickle_dir):
        os.makedirs(args.pickle_dir)

    connection = psycopg2.connect(
        # client_encoding = 'ascii',
        database = "udd",
        host = "localhost",
        password = "udd-guest",
        user = "udd-guest",
        )
    connection.set_client_encoding('LATIN1')

    log.info("Retrieving packages...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM packages
                ORDER BY package, version, architecture, distribution, release, component
                """)
            for row in cursor:
                entry = row_to_dict(row)
                assert entry.pop('distribution') == 'debian'
                package = part = load_package(entry.pop('package'))
                part = set_default_dict(part, 'releases')
                part = set_default_dict(part, entry.pop('release'))
                part = set_default_dict(part, entry.pop('component'))
                part = set_default_dict(part, 'versions')
                part = set_default_dict(part, entry.pop('version'))
                part = set_default_dict(part, 'architectures')
                part = set_unique(part, entry.pop('architecture'), entry)
                split_comma_in_item(part, 'breaks')
                split_comma_in_item(part, 'conflicts')
                split_comma_in_item(part, 'depends')
                del part['description']  # Redundant with descriptions provided below.
                split_comma_in_item(part, 'enhances')
                split_comma_in_item(part, 'pre_depends')
                split_comma_in_item(part, 'provides')
                split_comma_in_item(part, 'recommends')
                split_comma_in_item(part, 'replaces')
                split_comma_in_item(part, 'suggests')
                split_comma_in_item(part, 'tag')

                save_package(package)

    log.info("Retrieving sources...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM sources
                ORDER BY source, version, distribution, release, component
                """)
            for row in cursor:
                entry = row_to_dict(row)
                assert entry.pop('distribution') == 'debian'
                source = part = load_source(entry.pop('source'))
                part = set_default_dict(part, 'releases')
                part = set_default_dict(part, entry.pop('release'))
                part = set_default_dict(part, entry.pop('component'))
                part = set_default_dict(part, 'versions')
                part = set_default_dict(part, entry.pop('version'))
                part = set_default_dict(part, 'architectures')
                part = set_unique(part, entry.pop('architecture'), entry)
                split_comma_in_item(part, 'bin')
                split_comma_in_item(part, 'build_conflicts')
                split_comma_in_item(part, 'build_depends')
                split_comma_in_item(part, 'build_depends_indep')

                save_source(source)

    log.info("Retrieving descriptions...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM descriptions
                ORDER BY package
                """)
            for row in cursor:
                entry = row_to_dict(row)
                assert entry.pop('distribution') == 'debian'
                package = part = load_package(entry.pop('package'))
                part = set_default_dict(part, 'releases')
                part = set_default_dict(part, entry.pop('release'))
                part = set_default_dict(part, entry.pop('component'))
                part = set_default_dict(part, 'descriptions')
                part = set_default_dict(part, entry.pop('description_md5'))
                part = set_unique(part, entry.pop('language'), entry)
                encode_str_in_item(part, 'description')
                encode_str_in_item(part, 'long_description')

                save_package(package)

    log.info("Retrieving orphaned_packages...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM orphaned_packages
                ORDER BY source
                """)
            for row in cursor:
                entry = row_to_dict(row)
                source = part = load_source(entry.pop('source'))
                set_unique(part, 'orphaned', entry)

                save_source(source)

    log.info("Retrieving popcon_src...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM popcon_src
                ORDER BY source
                """)
            for row in cursor:
                entry = row_to_dict(row)
                source = part = load_source(entry.pop('source'))
                set_unique(part, 'popcon', entry)

                save_source(source)

    log.info("Retrieving popcon_src_average...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM popcon_src_average
                ORDER BY source
                """)
            for row in cursor:
                entry = row_to_dict(row)
                source = part = load_source(entry.pop('source'))
                set_unique(part, 'popcon_average', entry)

                save_source(source)

    log.info("Retrieving screenshots...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM screenshots
                """)
            for row in cursor:
                entry = row_to_dict(row)
                package = part = load_package(entry.pop('package'))
                screenshots = set_default_list(part, 'screenshots')
                screenshots.append(entry)

                save_package(package)

    log.info("Retrieving security_issues...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM security_issues
                """)
            for row in cursor:
                entry = row_to_dict(row)
                source = part = load_source(entry.pop('source'))
                part = set_default_dict(part, 'security_issues')
                set_unique(part, entry.pop('issue'), entry)

                save_source(source)

    log.info("Retrieving security_issues_releases...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM security_issues_releases
                ORDER BY source
                """)
            for row in cursor:
                entry = row_to_dict(row)
                source = part = load_source(entry.pop('source'))
                part = set_default_dict(part, 'security_issues')
                part = set_default_dict(part, entry.pop('issue'))
                set_unique(part, entry.pop('release'), entry)

                save_source(source)

    log.info("Retrieving upstream...")
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT * FROM upstream
                ORDER BY source, version, distribution, release, component
                """)
            for row in cursor:
                entry = row_to_dict(row)
                assert entry.pop('distribution') == 'debian'
                source = part = load_source(entry.pop('source'))
                part = set_default_dict(part, 'releases')
                part = set_default_dict(part, entry.pop('release'))
                part = set_default_dict(part, entry.pop('component'))
                part = set_default_dict(part, 'versions')
                part = set_default_dict(part, entry.pop('version'))
                set_unique(part, 'upstream', entry)

                save_source(source)

    save_packages()
    save_sources()
    connection.close()

    return 0


def row_to_dict(row):
    return {
        key: value
        for key, value in row.items()
        if value is not None
        }


def save_package(package):
    name = package['name']
    assert name is not None
    package_by_name[name] = package


def save_packages():
    while package_by_name:
        name, package = package_by_name.popitem()
        package_dir = os.path.join(
            args.pickle_dir,
            'packages',
            name[0:4] if name.startswith('lib') else name[0],
            )
        if not os.path.exists(package_dir):
            os.makedirs(package_dir)
        with open(os.path.join(package_dir, '{}.pickle'.format(name)), 'wb') as package_file:
            pickle.dump(package, package_file, pickle.HIGHEST_PROTOCOL)


def save_source(source):
    name = source['name']
    assert name is not None
    source_by_name[name] = source


def save_sources():
    while source_by_name:
        name, source = source_by_name.popitem()
        source_dir = os.path.join(
            args.pickle_dir,
            'sources',
            name[0:4] if name.startswith('lib') else name[0],
            )
        if not os.path.exists(source_dir):
            os.makedirs(source_dir)
        with open(os.path.join(source_dir, '{}.pickle'.format(name)), 'wb') as source_file:
            pickle.dump(source, source_file, pickle.HIGHEST_PROTOCOL)


def set_default_dict(d, key):
    item = d.get(key)
    if item is None:
        d[key] = item = {}
    return item


def set_default_list(d, key):
    item = d.get(key)
    if item is None:
        d[key] = item = []
    return item


def set_unique(d, key, value):
    assert key not in d or d[key] == value, pprint.pformat((key, d[key], value))
    if value is None:
        return None
    d[key] = value
    return d[key]


def split_comma(value):
    if value is None:
        return None
    return [
        fragment.strip()
        for fragment in value.split(',')
        ]


def split_comma_in_item(d, key):
    value = d.get(key)
    if value is not None:
        d[key] = split_comma(value)


if __name__ == "__main__":
    sys.exit(main())
