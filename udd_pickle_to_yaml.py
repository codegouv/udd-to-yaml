#! /usr/bin/env python3


# udd-to-yaml -- Convert Universal Debian Database to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/codegouv/udd-to-yaml
#
# udd-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# udd-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import collections
import logging
import os
import pickle
import sys

import yaml
try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


# YAML configuration


class folded_str(str):
    pass


class literal_str(str):
    pass


def dict_representer(dumper, data):
    return dumper.represent_dict(sorted(data.iteritems()))


yaml.add_representer(folded_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='>'))
yaml.add_representer(literal_str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str',
    data, style='|'))
yaml.add_representer(collections.OrderedDict, dict_representer)
yaml.add_representer(str, lambda dumper, data: dumper.represent_scalar(u'tag:yaml.org,2002:str', data))


#


app_name = os.path.splitext(os.path.basename(__file__))[0]
args = None
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('pickle_dir', help = 'path of source directory for pickle files')
    parser.add_argument('yaml_dir', help = 'path of target directory for generated YAML files')
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    global args
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    assert os.path.exists(args.pickle_dir), "Directory doesn't exist: {}".format(args.pickle_dir)
    if not os.path.exists(args.yaml_dir):
        os.makedirs(args.yaml_dir)

    for source_dir, dirs_name, filenames in os.walk(args.pickle_dir):
        for dir_name in dirs_name[:]:
            if dir_name.startswith('.'):
                dirs_name.remove(dir_name)
        target_dir = os.path.join(args.yaml_dir, os.path.relpath(source_dir, args.pickle_dir))
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        for filename in filenames:
            if not filename.endswith(".pickle"):
                continue
            with open(os.path.join(source_dir, filename), 'rb') as pickle_file:
                data = pickle.load(pickle_file)
            with open(os.path.join(target_dir, '{}.yaml'.format(os.path.splitext(filename)[0])), 'w') as yaml_file:
                yaml.dump(data, yaml_file, allow_unicode = True, default_flow_style = False, Dumper = Dumper,
                    indent = 2, width = 120)

    return 0


if __name__ == "__main__":
    sys.exit(main())
