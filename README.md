# udd-to-yaml

Convert Universal Debian Database (UDD) to a Git repository of YAML files.


## Install

_cf: https://wiki.debian.org/UltimateDebianDatabase/Hacking_

```bash
sudo apt-get install postgresql-9.4 postgresql-9.4-debversion
sudo su postgres
createuser udd
createuser --login --pwprompt udd-guest
  Enter password for new role: udd-guest
  Enter it again: udd-guest
exit
```

## Usage

_cf: https://wiki.debian.org/UltimateDebianDatabase/Hacking_

```bash
wget "https://udd.debian.org/dumps/udd.dump"
sudo su postgres
pg_restore --create --clean udd.dump | psql
# psql udd -c 'GRANT usage ON schema public TO PUBLIC'
# psql udd -c 'GRANT select ON all tables in schema public TO PUBLIC'
exit
git clone https://git.framasoft.org/codegouv/udd-to-yaml.git
cd udd-to-yaml
rm -Rf pickle/*
./udd_postgresql_to_pickle.py -a -v pickle/
rm -Rf yaml/*
./udd_pickle_to_yaml.py pickle/ yaml/
```

_TODO: Download & install other dump files from [https://udd.debian.org/dumps/](https://udd.debian.org/dumps/)?_
