#! /usr/bin/env python3


# udd-to-yaml -- Convert Universal Debian Database to a Git repository of YAML files
# By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
#
# Copyright (C) 2015 Etalab
# https://git.framasoft.org/codegouv/udd-to-yaml
#
# udd-to-yaml is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# udd-to-yaml is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


import argparse
import logging
import os
import sys

import psycopg2
import psycopg2.extras


app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
    args = parser.parse_args()
    logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)

    connection = psycopg2.connect(
        database = "udd",
        host = "localhost",
        password = "udd-guest",
        user = "udd-guest",
        )

    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT DISTINCT distribution
                FROM packages
                """)
            distributions = [
                row['distribution']
                for row in cursor
                ]
            assert distributions == ["debian"], distributions

    packages_name = None
    with connection:
        with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
            cursor.execute("""
                SELECT DISTINCT package
                FROM packages
                ORDER BY package
                """)
            packages_name = [
                row['package']
                for row in cursor
                ]

    # for package_name in packages_name:
    #     with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
    #         cursor.execute("""
    #             SELECT maintainer, maintainer_name, maintainer_email
    #             FROM packages
    #             """)
    #         for row in cursor:
    #             assert row["maintainer"] == "{} <{}>".format(row["maintainer_name"], row["maintainer_email"]), (
    #                 package_name, row["maintainer"], row["maintainer_name"], row["maintainer_email"])

    # with connection:
    #     for package_name in packages_name:
    #         with connection.cursor(cursor_factory = psycopg2.extras.DictCursor) as cursor:
    #             cursor.execute("""
    #                 SELECT DISTINCT maintainer, maintainer_name, maintainer_email
    #                 FROM packages
    #                 WHERE package = %s
    #                 """, [package_name])
    #             maintainers = set(
    #                 (row['maintainer'], row['maintainer_name'], row['maintainer_email'])
    #                 for row in cursor
    #                 )
    #             assert len(maintainers) == 1, (package_name, maintainers)

    connection.close()
    return 0


if __name__ == "__main__":
    sys.exit(main())
